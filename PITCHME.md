# Vim Tutorial

ResBaz Sydney, 2019
Aidan Wilson
Intersect, ACU
[aidan.wilson@intersect.org.au](mailto:aidan.wilson@intersect.org.au)

---

## Topics

- Why Vim?
- Opening, saving, not saving, and closing
- Modes: Normal, Insert, Visual
- Motions: Moving around
- Searching
- Editing
- Deleting, Yanking and Pasting
- Selecting text in Visual mode (and then doing stuff with it)
- Operators, Counts and Motions
- Text Objects
- Visual selections with motions and text objects
- Ex commands
- Where to from here?
- Bonus: External commands
- Bonus 2: Surround
- Miscellaneous tricks
- test

---

## Why Vim?

- Ubiquitous, available on almost every unix system
- Available as a GUI app called GVim
- Powerful and fast with terse, mnemonic commands and operations
- Fully within the terminal
- Not dependent on a desktop environment
- Highly customisable through configuration files
- Highly extensible through a wide range of plugins and modules available freely online

---

## Opening, saving, not saving, and closing

- `vim [filename|directory]` to open vim. Executing over a file will open that file for editing. Opening over a directory will open that directory in Vim's file browser.
- Once in Vim, `:q` to quit if in normal mode. Otherwise <kbd>ESC</kbd>, then `:q`.
- If you have modified a file and do not wish to save, append `!` to the quit command; `:q!`.
- Save a file with `:w`, and do both at once with `:wq`.
- `:x` also saves and quits, but only saves if changes have been made (and so doesn't update the _modified time_)

---

## Modes: Normal, Insert, Visual

Vim's modal interface is one of the hurdles to its use. It is just not intuitive to have to change mode in order to type text, and then change modes to do things like save and close. The modes are fairly straightforward and after even just a few minutes of tinkering, it becomes second nature.

- Normal (default mode):
  - Moving around the file
  - Yanking (copying) text, pasting text, deleting text
  - Searching through text
  - Running commands, like save, close, substitute, join

- Insert:
  - Enter Insert mode with `i`,`I`,`a`,`A`,`o`,`O`,`s` or `S` from Normal or Visual mode (each one does a different thing)
  - Typing in text
  - Return to Normal mode using <kbd>ESC</kbd>.

- Visual
  - Enter Visual mode with `v`,`V` or`^v` (<kbd>CTRL</kbd>+`v`) from Normal mode only.
  - Selecting characters, lines or blocks of text for some later kind of operation, such as yanking, deleting, replacing, surrounding, shifting left/right, switch case, etc.
  - Go back to Normal mode using <kbd>ESC</kbd>.

**Hint**: Generally a good idea to tap <kbd>ESC</kbd> to ensure you're in Normal mode before running commands, otherwise you may inadvertently delete stuff.

---

## Motions: Moving around

- `h`, `j`, `k` and `l` move cursor one characer or line _left_, _down_, _up_ and _right_ respectively (they also fit your four fingers very well on the home row)
- `H`, `M`, `L` move cursor to top, middle or bottom of screen (think _high_, _middle_ and _low_)
- `w`, `W` move forwards to the start of a word/WORD
- `e`, `E` move forwards to the end of a word/WORD
- `b`, `B` move backwards to the start of a word/WORD
- `0`, `$` move to the start, end of a line
- `^` move to first non-blank character
- `gg`, `G` move to the very beginning, end of the entire file
- `[N]G` move directly to line N (eg., `5G` move to line 5)

A _word_ in the above means a sequence of alphanumeric characters and underscores, OR a sequence of other non-blank characters. A _WORD_ consists of a sequence of non-blank characters. _Himawari-8_ is 3 _words_ but only 1 _WORD_.

Most of these commands can be quantified by a count, i.e., `7j`, move down 7 lines, `4b`, move backards 4 words, but motions like `gg` and `0` cannot accept counts.

**Hint**: Download or bookmark a cheat sheet, like [this one][vim.rtorr.com] or [this one][vimcheatsheet.com].

---

## Searching

- `/` commence a search. Search string can be literal text or regular expression. Searching is dynamic when entering search string, it will highlight and move cursor to the next match automatically.
- <kbd>ESC</kbd> will cancel the search and return your cursor to the earlier position
- <kbd>Enter</kbd> will execute the search and highlight all matches and move your cursor to the next match
- `n`, `N` navigate forwards/backwards through matches
- `fx` finds the next occurrence of 'x' (and moves there)
- `Fx` finds the previous occurrence of 'x'
- `tx` moves _until_ (mnemonic _to_) the next occurrence of 'x'
- `;`, `,` continue to next/previous match of a find (`f`, `F` or `t`)

---

## Editing

- Entering Insert mode will let you type in any text you want. All keys work as expected in Insert mode.
- Ways of entering Insert mode include:
  - `i` insert at the current cursor position
  - `a` insert after the current cursor position (append; useful with `ea`, move to end of current word and append there)
  - `I` insert at the beginning of the current line
  - `A` append to the end of the current line
  - `o` _open_ a new line after the current line and begin inserting there
  - `O` _open_ a new line before the current line and begin inserting ther
  - `s` substitute the current visual selection (or single character) and begin inserting
  - `S` substitute the current whole line and begin inserting

- Other ways of editing:
  - `r` replace a single character
  - `R` replace continually until you hit <kbd>ESC</kbd>, i.e. overwrite
  - `c` change something (requires a movement, like `cw`, change word, `c$` change to end of line), or a selection
  - `~` switch case of current character or selection
  - `u` undo
  - <kbd>CTRL</kbd>+`r` redo

---

## Deleting, Yanking and Pasting

- `x` delete a single character (try also e.g. `7x` to delete the next 7 characters)
- `d` delete a selection or motion
- `dd` delete a line, `5dd` and so on
- `y` yank (copy) a selection or motion
- `yy` yank (copy) the current line
- `p` and `P` paste the yanked or deleted text after/before the current position. For example, `xp` to transpose two characters, `ddggP` to move the current line to the start of the file, `yipGp` to duplicate the entire current paragraph to the end of the file.

Pasting will also work over a visual selection (see next section). Pasting while text is selected will replace that text with whatever is on the register (clipboard).

---

## Selecting text in Visual mode (and then doing stuff with it)

---

### Selecting stuff

- `v` enter character-wise selection mode. Move around with any of the movement commands or search commands listed above. I.e., `vf)` select all the text from the current position to the next occurrence of a close parentheses, `v/and`+<kbd>Enter</kbd> will select from the cursor until the 'and'. Continue the selection through additional matches with `n` (or backwards with `N`).
- `V` enter line-wise selection mode. Move around with `j` and `k` to broaden the search upwards or downwards. Also use any other movement or search.
- <kbd>CTRL</kbd>+`v` enter block-wise selection mode. Move around any way you want and select a block of text.
- `o` while in any Visual mode will move the cursor to the other end of the selection to refine the selection.

---

### Doing stuff with selections

- Any of the above, such as yanking, deleting, switching case, pasting (which will replace),
- Insert something at the start of all selected lines, say, to comment out a section of code, or wrap lines in `<li>` tags
- De/indent with `<`,`>`
- Paste over selected text from register (clipboard) with `p`

---

## Operators, Counts and Motions

Commands like `2dd`, delete two lines, consist of a count (`2`) and an operator (`dd`).

Commands can also consist of an operator (like `d`, `y`, `c`, `gU`) and a motion (like `w`, `$`, `G`), in which case the operation takes place over the text between the cursor and the target of the motion. For example:
- `c$` change to the end of the current line
- `de` delete to the end of the word
- `y/END` + <kbd>Enter</kbd> yank up to the next match for the search string "END"

Add a count, either before or after the operator, to quantify the operation and motion:
- `y4k` yank from the current line to four lines down
- `2gUw` make the next two words go uppercase
- `d4f}` delete from the current position up to and including 4 } characters away

---

## Text Objects

Text objects are commands that are used in visual mode or in conjunction with operators, and are used to select, or apply the operation to, a range of text as defined by some criteria. Text objects are just like motions, but rather than operating over the text between the current cursor position and the target of the motion, it operates over _all_ text within the object:
- `ci"` change inside double quotation marks
- `ya)` yank a parenthesis (everything inside the parentheses and the parentheses themselves)
- `gUip` capitalise the entire paragraph
- `dat` delete XML/HTML tag (and its contents)

Text objects start with `i` or `a`, where `i` means _inner_ or _inside_, and `a` means _around_ or _a/an_.

---

## Visual selections with motions and text objects

Visual selection can also take motions and text objects.
- `v$` select to the end of the line
- `VG` select (linewise) to the end of the file
- <kbd>CTRL</kbd>+`v`, `4l2j` select a block from the current position, going four characters to the right and two lines down (creating a 5x3 block)
- `v4w` select four words
- `vis` select inside the sentence
- `va"` select a quoted section

---

## Ex commands

Vim has another mode called Ex, or Execute, for executing commands in its own language. These are different from the operators discussed so far but a lot of time their functions overlap. Ex commands tend to work on whole lines and take an address before the command, which is the line/s they operate over.
Ex commands include `:d` delete, `:m` move, `:t` copy, `:s` substitute and `:p` print.

- `:10,15p` print lines 10 to 15
- `:.,$d` delete from the current line until the end of the file
- `:s/foo/bar` replace first instance of 'foo' with 'bar' in the current line
- `:%s/[0-9]/x/g` replace all numbers with 'x' in the file
- `:%s/ *^//` remove all trailing whitespace 
- `:16,19t.` copy lines 16 through to 19 and insert after the current line (useful for duplicating a section of text, like a TOC slide)

The Global command `:g` takes a regular expression, and all lines that match that reguar expresion will be operated on by the command.
- `:g/^$/d` delete all empty lines
- `:g/^/m0` reverse all lines in a file
- `:g/[^.]$/j` join all split lines in a text
- `:g/./norm o` intersperse a newline (if a line contains anything, enter normal mode and open a newline below)
- ---
- 
- `:g/## /p` print all lines that start with "## " (i.e., level 2 headings in a markdown file)

The last of these reveals the origin of the name _grep_: `g/re/p`.

---

## Where to from here?

- `vivtutor` is a quick tutorial that should come installed with Vim, and goes through much of what I've covered, and probably more.
- Get a Vim cheat sheet and stick it to your wall.
- If you get sucked into the vim universe, you'll begin looking for applications that have or support vim keybindings.
- RStudio's editor pane can be switched to vim keybindings
- QuteBrowser is a web browser that is built on vim keybindings. Navigate the web without touching the mouse!
- Notetaking software, like Boostnote, supports vim.

---

## Bonus: External commands

`!` allows you to run text through an external command line program.
- `:10,20!sort|uniq` sort lines 10-20 and remove duplicate lines
- `:.,+3!tail -r` reverse the order of lines from the current to 3 lines forward

`:r!` allows you to run an external command and paste the output at the current position (or desired position)
- `:0r!date +\%Y-\%M-\%d` insert today's date at the top of the file
- `:r!pwd` insert the current working directory at the current position

---

## Bonus 2: Surround

Vim-Surround is a plugin that adds an operator to work with surrounds, by surrounding text with some character or pair of characters, remove the surrounding characters, or change between surround types. Types include quote marks of all types, parentheses, braces and brackets, XML/HTML tags, and even LaTeX tags.

- `ysiw"` surround the current word with double quotation marks
- `cs"'` change surrounds from double to single quotation marks
- `ds'` delete surrounding single quotation marks

Surround works with visually selected text and is a little easier:
- `S)` surround selected text with parentheses
- `Stli`+<kbd>Enter</kbd> surround selected text with `<li>` tags
- `SlDocument`+<kbd>Enter</kbd> surround selected text with `\begin{Document}` and `\end{Document}`

---

## Miscellaneous tricks

- `K` in normal mode will open the man page or contextual help (like python help page) for the word under the cursor
- `%` in normal mode while your cursor sits on a parenthesis, bracket or brace will move you to the opposite matching character

[vim.rtorr.com]: https://vim.rtorr.com/
[vimcheatsheet.com]: https://cdn.shopify.com/s/files/1/0165/4168/files/preview.png


